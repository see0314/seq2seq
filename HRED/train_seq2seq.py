# coding: utf-8
import datetime
import json
import sys
import pandas as pd
from chainer import optimizer, optimizers, serializers
from Bi_seq2seq import *

argvs = sys.argv
input = argvs[1]
output = argvs[2]
batch = argvs[3]


# trainの関数
def make_minibatch(minibatch):
    # enc_wordsの作成
    enc_words = [row[0] for row in minibatch]
    enc_max = np.max([len(row) for row in enc_words])
    enc_words = np.array([[-1] * (enc_max - len(row)) + row for row in enc_words], dtype='int32')
    enc_words = enc_words.T

    # dec_wordsの作成
    dec_words = [row[1] for row in minibatch]
    dec_max = np.max([len(row) for row in dec_words])
    dec_words = np.array([row + [-1] * (dec_max - len(row)) for row in dec_words], dtype='int32')
    dec_words = dec_words.T
    return enc_words, dec_words


def train():
    input_data = pd.read_table("./dataset_shori/" + input, encoding='utf-16', header=None)
    output_data = pd.read_table("./dataset_shori/" + output, encoding='utf-16', header=None)
    input_dataset = []
    output_dataset = []
    dataset = []

    for index, row in input_data.iterrows():
        input_dataset.append(row[0])

    for index, row in output_data.iterrows():
        output_dataset.append(row[0])

    for i in range(len(input_dataset)):
        dataset.append([input_dataset[i].split(","), output_dataset[i].split(",")])

    N = len(dataset)
    print("data load")
    del dataset
    del input_data
    del input_dataset
    del output_data
    del output_dataset

    f = open("./dataset_shori/dictionary.json", 'r')
    mydict = json.load(f)
    # 語彙数
    vocab_size = len(mydict.keys())
    print("dictionary generate")
    del mydict

    # 転移学習元のモデル
    model = Seq2Seq(vocab_size=vocab_size,
                    embed_size=EMBED_SIZE,
                    hidden_size=HIDDEN_SIZE,
                    batch_size=BATCH_SIZE,
                    flag_gpu=FLAG_GPU)
    PATH = './original_model/result_EMBED%s_HIDDEN%s_BATCH%s_EPOCH%s.weights' \
           % (EMBED_SIZE, HIDDEN_SIZE, BATCH_SIZE_original, 40)
    serializers.load_hdf5(PATH, model)

    model.reset()
    # GPUのセット
    if FLAG_GPU:
        ARR = cuda.cupy
        cuda.get_device(0).use()
        model.to_gpu(0)
    else:
        ARR = np

    # 学習開始
    for epoch in range(EPOCH_NUM):
        # エポックごとにoptimizerの初期化
        sum_loss = 0
        sum_accuracy = 0
        opt = optimizers.Adam()
        opt.setup(model)
        opt.add_hook(optimizer.GradientClipping(5))
        for num in range(N // BATCH_SIZE):
            print(str(num * BATCH_SIZE))
            input_data = pd.read_table("./dataset_shori/" + input, encoding='utf-16', header=None).loc[
                         num * BATCH_SIZE: (num + 1) * BATCH_SIZE - 1]
            output_data = pd.read_table("./dataset_shori/" + output, encoding='utf-16', header=None).loc[
                          num * BATCH_SIZE: (num + 1) * BATCH_SIZE - 1]
            input_dataset = []
            output_dataset = []
            dataset = []

            for index, row in input_data.iterrows():
                input_dataset.append(row[0])

            for index, row in output_data.iterrows():
                output_dataset.append(row[0])

            for i in range(len(input_dataset)):
                dataset.append([input_dataset[i].split(","), output_dataset[i].split(",")])

            # 読み込み用のデータ作成
            enc_words, dec_words = make_minibatch(dataset)
            # modelのリセット
            model.reset()
            # 順伝播
            total_loss, accuracy = forward(enc_words=enc_words,
                                           dec_words=dec_words,
                                           model=model,
                                           ARR=ARR)

            sum_loss += total_loss.data
            sum_accuracy += accuracy.data
            # 学習
            total_loss.backward()
            opt.update()
            opt.reallocate_cleared_grads()

        print('Epoch %s 終了' % (epoch + 1))
        print('loss = %s' % (sum_loss / N))
        print('accuracy = %s' % (sum_accuracy / N))

        f = open('./model/result.txt', 'a')
        stri = ('Epoch %s 終了' % (epoch + 1)) + (
                ', loss = %s' % (sum_loss / N) + ', accuracy = %s' % (sum_accuracy / N) + '終了時間 = ' + str(
            datetime.datetime.now()))
        stri = stri + "\n"
        f.write(stri)
        f.close()

        outputpath = OUTPUT_PATH % (EMBED_SIZE, HIDDEN_SIZE, BATCH_SIZE, epoch + 1)
        serializers.save_hdf5(outputpath, model)


OUTPUT_PATH = './model/result_EMBED%s_HIDDEN%s_BATCH%s_EPOCH%s.weights'
FLAG_GPU = True

EMBED_SIZE = 300
HIDDEN_SIZE = 150
BATCH_SIZE = int(batch)
EPOCH_NUM = 30
BATCH_SIZE_original = 18

username = 'Seq2Seq'

print('開始: ', datetime.datetime.now())

train()

print('終了: ', datetime.datetime.now())
