# coding: utf-8
import numpy as np
from chainer import Chain, Variable, cuda, functions, links


class LSTM_Encoder_f(Chain):
    def __init__(self, vocab_size, embed_size, hidden_size):
        """
        クラスの初期化
        :param vocab_size: 使われる単語の種類数（語彙数）
        :param embed_size: 単語をベクトル表現した際のサイズ
        :param hidden_size: 隠れ層のサイズ
        """
        super(LSTM_Encoder_f, self).__init__(
            xe=links.EmbedID(vocab_size, embed_size, ignore_label=-1),
            eh=links.Linear(embed_size, 4 * hidden_size),
            hh=links.Linear(hidden_size, 4 * hidden_size)
        )

    def __call__(self, x, c_forward, h_forward):
        """
        :param x: one-hotな単語
        :param c: 内部メモリ
        :param h: 隠れ層
        :return: 次の内部メモリ、次の隠れ層
        """
        e = functions.tanh(self.xe(x))
        return functions.lstm(c_forward, self.eh(e) + self.hh(h_forward))


class LSTM_Encoder_b(Chain):
    def __init__(self, vocab_size, embed_size, hidden_size):
        """
        クラスの初期化
        :param vocab_size: 使われる単語の種類数（語彙数）
        :param embed_size: 単語をベクトル表現した際のサイズ
        :param hidden_size: 隠れ層のサイズ
        """
        super(LSTM_Encoder_b, self).__init__(
            xe=links.EmbedID(vocab_size, embed_size, ignore_label=-1),
            eh=links.Linear(embed_size, 4 * hidden_size),
            hh=links.Linear(hidden_size, 4 * hidden_size)
        )

    def __call__(self, x, c_backward, h_backward):
        """
        :param x: one-hotな単語
        :param c: 内部メモリ
        :param h: 隠れ層
        :return: 次の内部メモリ、次の隠れ層
        """
        e = functions.tanh(self.xe(x))
        return functions.lstm(c_backward, self.eh(e) + self.hh(h_backward))


class LSTM_Decoder(Chain):
    def __init__(self, vocab_size, embed_size, hidden_size):
        """
        クラスの初期化
        :param vocab_size: 使われる単語の種類数（語彙数）
        :param embed_size: 単語をベクトル表現した際のサイズ
        :param hidden_size: 隠れ層のサイズ
        """
        super(LSTM_Decoder, self).__init__(
            ye=links.EmbedID(vocab_size, embed_size, ignore_label=-1),
            eh=links.Linear(embed_size, 4 * hidden_size),
            hh=links.Linear(hidden_size, 4 * hidden_size),
            he=links.Linear(hidden_size, embed_size),
            ey=links.Linear(embed_size, vocab_size)
        )

    def __call__(self, y, c, h):
        """
        :param y: one-hotな単語
        :param c: 内部メモリ
        :param h: 隠れそう
        :return: 予測単語、次の内部メモリ、次の隠れ層
        """
        e = functions.tanh(self.ye(y))
        c, h = functions.lstm(c, self.eh(e) + self.hh(h))
        t = self.ey(functions.tanh(self.he(h)))
        return t, c, h


class LSTM_Context(Chain):
    def __init__(self, embed_size, hidden_size):
        """
        クラスの初期化
        :param hidden_size: 隠れ層のサイズ
        """
        super(LSTM_Context, self).__init__(
            h_io=links.Linear(hidden_size, 4 * hidden_size),
            h_con=links.Linear(hidden_size, 4 * hidden_size),
        )

    def __call__(self, h, c, h_context):
        """
        :param h: encoderの出力
        :param c: 内部メモリ
        :param h_context: contextの隠れ層
        :return: 次の内部メモリ、次の隠れ層
        """
        c, h = functions.lstm(c, self.h_io(h) + self.h_con(h_context))
        return c, h


class Seq2Seq_VH(Chain):
    def __init__(self, vocab_size, embed_size, hidden_size, batch_size, flag_gpu=True):
        """
        Seq2Seqの初期化
        :param vocab_size: 語彙サイズ
        :param embed_size: 単語ベクトルのサイズ
        :param hidden_size: 中間ベクトルのサイズ
        :param batch_size: ミニバッチのサイズ
        :param flag_gpu: GPUを使うかどうか
        """
        super(Seq2Seq_VH, self).__init__(
            # Encoderのインスタンス化
            encoder_f=LSTM_Encoder_f(vocab_size, embed_size, hidden_size),
            encoder_b=LSTM_Encoder_b(vocab_size, embed_size, hidden_size),
            contextlstm=LSTM_Context(embed_size, hidden_size),
            # Decoderのインスタンス化
            decoder=LSTM_Decoder(vocab_size, embed_size, hidden_size),
            # hが2倍になるので半分にする
            h2h=links.Linear(hidden_size * 2, hidden_size)
        )
        self.vocab_size = vocab_size
        self.embed_size = embed_size
        self.hidden_size = hidden_size
        self.batch_size = batch_size
        # GPUで計算する場合はcupyをCPUで計算する場合はnumpyを使う
        if flag_gpu:
            self.ARR = cuda.cupy
        else:
            self.ARR = np

    def encode(self, words):
        """
        Encoderを計算する部分
        :param words: 単語が記録されたリスト
        :return:
        """
        # 内部メモリ、中間ベクトルの初期化
        h = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))

        c_forward = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))
        h_forward = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))

        c_backward = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))
        h_backward = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))

        # エンコーダーに単語を順番に読み込ませる
        for w_forward in words:
            c_forward, h_forward = self.encoder_f(w_forward, c_forward, h_forward)

        for w_backward in words[::-1]:
            c_backward, h_backward = self.encoder_b(w_backward, c_backward, h_backward)

        # 計算した中間ベクトルをデコーダーに引き継ぐためにインスタンス変数にする
        h = functions.concat((h_forward, h_backward[::-1]))
        h = functions.tanh(self.h2h(h))
        self.h = h
        # 内部メモリは引き継がないので、初期化
        self.c_forward = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))
        self.c_backward = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))
        return h

    def context(self, z):
        c, h = self.contextlstm(self.h, self. c, self.h_context)
        h_con = functions.concat((h, z), axis=1)
        h_con = functions.tanh(self.h2h(h_con))
        return h_con

    def context_test(self, z):
        c, h = self.contextlstm(self.h, self. c, self.h_context)
        h_con = functions.concat((h, z), axis=1)
        h_con = functions.tanh(self.h2h(h_con))
        return c, h_con

    def decode(self, w, h):
        """
        デコーダーを計算する部分
        :param w: 単語
        :return: 単語数サイズのベクトルを出力する
        """
        t, self.c, h = self.decoder(w, self.c, h)
        return t, h

    def decode_test(self, w, h):
        """
        デコーダーを計算する部分
        :param w: 単語
        :return: 単語数サイズのベクトルを出力する
        """
        t, self.c, h = self.decoder(w, self.c, h)
        c = self.c
        return t, h, c

    def reset(self):
        """
        中間ベクトル、内部メモリ、勾配の初期化
        :return:
        """
        self.h = Variable(self.ARR.zeros((self.batch_size, self.hidden_size*2), dtype='float32'))
        self.h_context = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))
        self.h_forward = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))
        self.h_backward = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))
        self.c = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))
        self.c_forward = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))
        self.c_backward = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))

        self.zerograds()


def KL(prior_mean, prior_var, posterior_mean, posterior_var, ARR):
    prior_mean = ARR.matrix(prior_mean)
    prior_var = ARR.matrix(prior_var)
    posterior_mean = ARR.matrix(posterior_mean)
    posterior_var = ARR.matrix(posterior_var)

    a = ARR.log((ARR.linalg.det(posterior_var) / ARR.linalg.det(prior_var)))
    b = ARR.trace(posterior_var.I * prior_var)
    c =(prior_mean - posterior_mean) * posterior_var.I * (prior_mean - posterior_mean).T
    d = ARR.trace(prior_var.I * prior_var)
    return 0.5 * (a+b+c-d)


def KL_onedimention(prior_mean, prior_var, posterior_mean, posterior_var, ARR):
    a = ARR.log(posterior_var / prior_var)
    # print("a = " + str(a))
    b = prior_var / posterior_var
    # print("b = " + str(b))
    c = ((prior_mean - posterior_mean) ** 2) / posterior_var
    # print("c = " + str(c))
    return 0.5 * (a+b+c-1)


def forward_VH(enc_words, dec_words, model, ARR):
    """
    順伝播の計算を行う関数
    :param enc_words: 発話文の単語を記録したリスト
    :param dec_words: 応答文の単語を記録したリスト
    :param model: Seq2Seqのインスタンス
    :param ARR: cuda.cupyかnumpyか
    :return: 計算した損失の合計
    """
    # バッチサイズを記録
    batch_size = len(enc_words[0])
    # model内に保存されている勾配をリセット
    model.reset()
    # 発話リスト内の単語を、chainerの型であるVariable型に変更
    enc_words = [Variable(ARR.array(row, dtype='int32')) for row in enc_words]
    # エンコードの計算 ①
    h_batch = model.encode(enc_words)

    # KLとzの初期化
    kl_divergence_cost = 0
    z_matrix = []
    for i in range(batch_size):
        # if i == 0:
        if i == (batch_size-1):
            posterior_mean = ARR.mean(h_batch[:i+1].data, axis=0)
            posterior_var = ARR.var(h_batch[:i+1].data, axis=0)
            # zの計算
            z = ARR.random.normal(posterior_mean, posterior_var, (1, 150)).astype("f4")
            z_matrix.append(z[0])
        else:
            mean_now = ARR.mean(h_batch[:i+1].data)
            var_now = ARR.var(h_batch[:i+1].data)

            mean_next = ARR.mean(h_batch[:i+2].data)
            var_next = ARR.var(h_batch[:i+2].data)

            # KLの計算
            kl_divergence_cost += KL_onedimention(mean_now, var_now, mean_next, var_next, ARR)

            posterior_mean = ARR.mean(h_batch[:i+2].data, axis=0)
            posterior_var = ARR.var(h_batch[:i+2].data, axis=0)
            # zの計算
            z = ARR.random.normal(posterior_mean, posterior_var, (1, 150)).astype("f4")
            z_matrix.append(z[0])
        # else:
        #     print(i)
        #     # 現在と次の発話のhの平均分散をそれぞれ計算
        #     prior_mean = ARR.mean(h_batch[:i+1].data, axis=0)
        #     prior_var = ARR.var(h_batch[:i+1].data, axis=0)
        #     prior_cov = ARR.cov(h_batch[:i+1].data, rowvar=0)
        #     posterior_mean = ARR.mean(h_batch[:i+2].data, axis=0)
        #     posterior_var = ARR.var(h_batch[:i+2].data, axis=0)
        #     posterior_cov = ARR.cov(h_batch[:i+2].data, rowvar=0)
        #     # KLの計算
        #     kl_divergence_cost += KL(prior_mean, prior_cov, posterior_mean, posterior_cov, ARR)
        #     # zの計算
        #     z = ARR.random.normal(posterior_mean, posterior_var, (1, 150)).astype("f4")
        #     z_matrix.append(z[0])
    z_matrix = Variable(ARR.array(z_matrix, dtype='float32'))
    # context層での計算
    h_old = model.context(z_matrix)
    # 損失と精度の初期化
    loss = Variable(ARR.zeros((), dtype='float32'))
    accuracy = Variable(ARR.zeros((), dtype='float32'))
    # <eos>をデコーダーに読み込ませる ②
    t = Variable(ARR.array([0 for _ in range(batch_size)], dtype='int32'))
    # デコーダーの計算
    for w in dec_words:
        # 1単語ずつをデコードする ③
        y, h_new = model.decode(t, h_old)
        # 正解単語をVariable型に変換
        t = Variable(ARR.array(w, dtype='int32'))
        # 正解単語と予測単語を照らし合わせて損失を計算 ④
        loss += functions.softmax_cross_entropy(y, t)
        accuracy += functions.accuracy(y, t)
        h_old = h_new
    print("softmax_loss = " + str(loss))
    print("kl_divergence_cost = " + str(kl_divergence_cost))
    loss += kl_divergence_cost
    return loss, accuracy


def forward_test_VH(enc_words, model, ARR, beamsearch, h_history, session_num):
    ret = []
    model.reset()
    enc_words = [Variable(ARR.array([row], dtype='int32')) for row in enc_words]

    if beamsearch:
        max_length = 50
        beam_width = 3
        h_batch = model.encode(enc_words)
        h_history.extend(h_batch)
        mean = ARR.mean(h_history[:session_num][0].data, axis=0)
        var = ARR.var(h_history[:session_num][0].data, axis=0)
        # zの計算
        z_matrix = []
        z = ARR.random.normal(mean, var, (1, 150)).astype("f4")
        z_matrix.append(z[0])
        z_matrix = Variable(ARR.array(z_matrix, dtype='float32'))
        new_cell_states, new_hidden_states = model.context_test(z_matrix)

        heaps = [[] for _ in range(max_length + 1)]

        # (score, translation, hidden_states, cell_states)
        heaps[0].append((0, [0], new_hidden_states, new_cell_states))

        solution = []
        solution_score = 1e8

        for i in range(max_length):
            heaps[i] = sorted(heaps[i], key=lambda t: t[0])[:beam_width]
            for score, translation, i_hidden_states, i_cell_states in heaps[i]:
                t = Variable(ARR.array([translation[-1]], dtype='int32'))
                output, new_hidden_states, new_cell_states = model.decode_test(t, new_hidden_states)
                output = functions.softmax(output)
                output = output[0]

                for next_wid in ARR.argsort(output.data)[::-1]:
                    if output.data[next_wid] < 1e-6:
                        solution = translation[1:]
                        break
                    next_score = score - ARR.log(output.data[next_wid])
                    if next_score > solution_score:
                        solution = translation[1:]
                        break
                    next_translation = translation + [next_wid]
                    next_item = (next_score, next_translation, new_hidden_states, new_cell_states)
                    if next_wid == 0:
                        if next_score < solution_score:
                            solution = translation[1:]  # [1:] drops first EOS
                            solution_score = next_score
                    else:
                        heaps[i + 1].append(next_item)
        for i in range(len(heaps)):
            if heaps[i] == []:
                continue
            else:
                score, translation, i_hidden_states, i_cell_states = heaps[i][0]
        return solution, h_history
    else:
        h_batch = model.encode(enc_words)
        h_history.extend(h_batch)
        mean = ARR.mean(h_history[:session_num][0].data, axis=0)
        var = ARR.var(h_history[:session_num][0].data, axis=0)
        # zの計算
        z_matrix = []
        z = ARR.random.normal(mean, var, (1, 150)).astype("f4")
        z_matrix.append(z[0])
        z_matrix = Variable(ARR.array(z_matrix, dtype='float32'))
        h_old = model.context(z_matrix)

        t = Variable(ARR.array([0], dtype='int32'))
        counter = 0
        while counter < 50:
            y, h_new = model.decode(t, h_old)
            label = y.data.argmax()
            ret.append(label)
            t = Variable(ARR.array([label], dtype='int32'))
            counter += 1
            if label == 0:
                counter = 50
            h_old = h_new
        return ret, h_history
