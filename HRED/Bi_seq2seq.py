# coding: utf-8
import numpy as np
from chainer import Chain, Variable, cuda, functions, links


class LSTM_Encoder_f(Chain):
    def __init__(self, vocab_size, embed_size, hidden_size):
        """
        クラスの初期化
        :param vocab_size: 使われる単語の種類数（語彙数）
        :param embed_size: 単語をベクトル表現した際のサイズ
        :param hidden_size: 隠れ層のサイズ
        """
        super(LSTM_Encoder_f, self).__init__(
            xe=links.EmbedID(vocab_size, embed_size, ignore_label=-1),
            eh=links.Linear(embed_size, 4 * hidden_size),
            hh=links.Linear(hidden_size, 4 * hidden_size)
        )

    def __call__(self, x, c_forward, h_forward):
        """
        :param x: one-hotな単語
        :param c: 内部メモリ
        :param h: 隠れ層
        :return: 次の内部メモリ、次の隠れ層
        """
        e = functions.tanh(self.xe(x))
        return functions.lstm(c_forward, self.eh(e) + self.hh(h_forward))


class LSTM_Encoder_b(Chain):
    def __init__(self, vocab_size, embed_size, hidden_size):
        """
        クラスの初期化
        :param vocab_size: 使われる単語の種類数（語彙数）
        :param embed_size: 単語をベクトル表現した際のサイズ
        :param hidden_size: 隠れ層のサイズ
        """
        super(LSTM_Encoder_b, self).__init__(
            xe=links.EmbedID(vocab_size, embed_size, ignore_label=-1),
            eh=links.Linear(embed_size, 4 * hidden_size),
            hh=links.Linear(hidden_size, 4 * hidden_size)
        )

    def __call__(self, x, c_backward, h_backward):
        """
        :param x: one-hotな単語
        :param c: 内部メモリ
        :param h: 隠れ層
        :return: 次の内部メモリ、次の隠れ層
        """
        e = functions.tanh(self.xe(x))
        return functions.lstm(c_backward, self.eh(e) + self.hh(h_backward))


class LSTM_Decoder(Chain):
    def __init__(self, vocab_size, embed_size, hidden_size):
        """
        クラスの初期化
        :param vocab_size: 使われる単語の種類数（語彙数）
        :param embed_size: 単語をベクトル表現した際のサイズ
        :param hidden_size: 隠れ層のサイズ
        """
        super(LSTM_Decoder, self).__init__(
            ye=links.EmbedID(vocab_size, embed_size, ignore_label=-1),
            eh=links.Linear(embed_size, 4 * hidden_size),
            hh=links.Linear(hidden_size, 4 * hidden_size),
            he=links.Linear(hidden_size, embed_size),
            ey=links.Linear(embed_size, vocab_size)
        )

    def __call__(self, y, c, h):
        """
        :param y: one-hotな単語
        :param c: 内部メモリ
        :param h: 隠れそう
        :return: 予測単語、次の内部メモリ、次の隠れ層
        """
        e = functions.tanh(self.ye(y))
        c, h = functions.lstm(c, self.eh(e) + self.hh(h))
        t = self.ey(functions.tanh(self.he(h)))
        return t, c, h


class Seq2Seq(Chain):
    def __init__(self, vocab_size, embed_size, hidden_size, batch_size, flag_gpu=True):
        """
        Seq2Seqの初期化
        :param vocab_size: 語彙サイズ
        :param embed_size: 単語ベクトルのサイズ
        :param hidden_size: 中間ベクトルのサイズ
        :param batch_size: ミニバッチのサイズ
        :param flag_gpu: GPUを使うかどうか
        """
        super(Seq2Seq, self).__init__(
            # Encoderのインスタンス化
            encoder_f=LSTM_Encoder_f(vocab_size, embed_size, hidden_size),
            encoder_b=LSTM_Encoder_b(vocab_size, embed_size, hidden_size),
            # Decoderのインスタンス化
            decoder=LSTM_Decoder(vocab_size, embed_size, hidden_size),
            # hが2倍になるので半分にする
            h2h=links.Linear(hidden_size * 2, hidden_size)
        )
        self.vocab_size = vocab_size
        self.embed_size = embed_size
        self.hidden_size = hidden_size
        self.batch_size = batch_size
        # GPUで計算する場合はcupyをCPUで計算する場合はnumpyを使う
        if flag_gpu:
            self.ARR = cuda.cupy
        else:
            self.ARR = np

    def encode(self, words):
        """
        Encoderを計算する部分
        :param words: 単語が記録されたリスト
        :return:
        """
        # 内部メモリ、中間ベクトルの初期化
        h = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))

        c_forward = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))
        h_forward = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))

        c_backward = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))
        h_backward = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))

        # エンコーダーに単語を順番に読み込ませる
        for w_forward in words:
            c_forward, h_forward = self.encoder_f(w_forward, c_forward, h_forward)

        for w_backward in words[::-1]:
            c_backward, h_backward = self.encoder_b(w_backward, c_backward, h_backward)

        # 計算した中間ベクトルをデコーダーに引き継ぐためにインスタンス変数にする
        h = functions.concat((h_forward, h_backward[::-1]))
        h = functions.tanh(self.h2h(h))
        self.h = h
        print(h)
        # 内部メモリは引き継がないので、初期化
        self.c_forward = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))
        self.c_backward = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))

    def decode(self, w):
        """
        デコーダーを計算する部分
        :param w: 単語
        :return: 単語数サイズのベクトルを出力する
        """
        t, self.c, self.h = self.decoder(w, self.c, self.h)
        return t

    def reset(self):
        """
        中間ベクトル、内部メモリ、勾配の初期化
        :return:
        """
        self.h = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))
        self.h_forward = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))
        self.h_backward = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))
        self.c = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))
        self.c_forward = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))
        self.c_backward = Variable(self.ARR.zeros((self.batch_size, self.hidden_size), dtype='float32'))

        self.zerograds()


def forward(enc_words, dec_words, model, ARR):
    """
    順伝播の計算を行う関数
    :param enc_words: 発話文の単語を記録したリスト
    :param dec_words: 応答文の単語を記録したリスト
    :param model: Seq2Seqのインスタンス
    :param ARR: cuda.cupyかnumpyか
    :return: 計算した損失の合計
    """
    # バッチサイズを記録
    batch_size = len(enc_words[0])
    # model内に保存されている勾配をリセット
    model.reset()
    # 発話リスト内の単語を、chainerの型であるVariable型に変更
    enc_words = [Variable(ARR.array(row, dtype='int32')) for row in enc_words]
    # エンコードの計算 ①
    model.encode(enc_words)
    # 損失の初期化
    loss = Variable(ARR.zeros((), dtype='float32'))
    accuracy = Variable(ARR.zeros((), dtype='float32'))
    # <eos>をデコーダーに読み込ませる ②
    t = Variable(ARR.array([0 for _ in range(batch_size)], dtype='int32'))
    # デコーダーの計算
    for w in dec_words:
        # 1単語ずつをデコードする ③
        y = model.decode(t)
        # 正解単語をVariable型に変換
        t = Variable(ARR.array(w, dtype='int32'))
        # 正解単語と予測単語を照らし合わせて損失を計算 ④
        loss += functions.softmax_cross_entropy(y, t)
        accuracy += functions.accuracy(y, t)
    print("softmax_loss = " + str(loss))
    return loss, accuracy


def forward_test(enc_words, model, ARR, beamsearch):
    ret = []
    model.reset()
    enc_words = [Variable(ARR.array([row], dtype='int32')) for row in enc_words]
    model.encode(enc_words)
    t = Variable(ARR.array([0], dtype='int32'))
    counter = 0
    while counter < 50:
        y = model.decode(t)
        label = y.data.argmax()
        ret.append(label)
        t = Variable(ARR.array([label], dtype='int32'))
        counter += 1
        if label == 0:
            counter = 50
    return ret
