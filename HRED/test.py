# coding: utf-8
import json
import sys

import pandas as pd

from vhred import *
from chainer import serializers

argvs = sys.argv
batch = argvs[1]
epoch = argvs[2]
beam = argvs[3]

EMBED_SIZE = 300
HIDDEN_SIZE = 150
BATCH_SIZE = batch
EPOCH_NUM = epoch
FLAG_GPU = True

if beam == "beam":
    beam = True
else:
    beam = False


def maxlen(minibatch):
    enc_words = [row[0] for row in minibatch]
    enc_max = np.max([len(row) for row in enc_words])

    return enc_max


input_data = pd.read_table("./dataset_shori/Utterance_id.txt", encoding='utf-16', header=None)
output_data = pd.read_table("./dataset_shori/Response_id.txt", encoding='utf-16', header=None)
input_dataset = []
output_dataset = []
dataset = []

for index, row in input_data.iterrows():
    input_dataset.append(row[0])

for index, row in output_data.iterrows():
    output_dataset.append(row[0])

for i in range(len(input_dataset)):
    dataset.append([input_dataset[i].split(","), output_dataset[i].split(",")])

max = maxlen(dataset)
del dataset
del input_data
del input_dataset
del output_data
del output_dataset

f = open("./dataset_shori/dictionary.json", 'r')
mydict = json.load(f)
f_inv = open("./dataset_shori/dictionary_inv.json", 'r')
mydict_inv = json.load(f_inv)
# 語彙数
vocab_size = len(mydict.keys())

model = Seq2Seq_VH(vocab_size=vocab_size,
                   embed_size=EMBED_SIZE,
                   hidden_size=HIDDEN_SIZE,
                   batch_size=1,
                   flag_gpu=FLAG_GPU)

if FLAG_GPU:
    ARR = cuda.cupy
    cuda.get_device(0).use()
    model.to_gpu(0)
else:
    ARR = np

PATH = './model_finetuning_done/result_EMBED%s_HIDDEN%s_BATCH%s_EPOCH%s.weights' \
       % (EMBED_SIZE, HIDDEN_SIZE, BATCH_SIZE, EPOCH_NUM)
serializers.load_hdf5(PATH, model)

h_history = []
session_num = 1

while 1:
    enc_words = []

    user_word = input('単語を入力　>> ')
    # user_word = "よろしくお願いします"
    enc_words.append(list(user_word))

    for i in range(len(enc_words)):
        for j in range(len(enc_words[i])):
            if enc_words[i][j] in mydict.keys():
                enc_words[i][j] = mydict[enc_words[i][j]]
            else:
                enc_words[i][j] = -1

    enc_words = np.array([[-1] * (max - len(enc_words[0])) + enc_words[0]])
    enc_words = np.array(enc_words).T

    result, h = forward_test_VH(enc_words=enc_words,
                                model=model,
                                ARR=ARR,
                                beamsearch=beam,
                                h_history=h_history,
                                session_num=session_num)

    h_history = h
    session_num += 1

    ans_inv = []
    for i in range(len(result)):
        word = mydict_inv[str(result[i])]
        ans_inv.append(word)
    print(ans_inv)

    # for i in range(len(result)):
    #     for k, l in mydict.items():
    #         if l == result[i]:
    #             ans.append(k)
    #             continue
    # print(ans)