# coding: utf-8
import json
import sys

import pandas as pd
import MeCab

from seq2seq import *
from chainer import serializers

argvs = sys.argv
batch = argvs[1]
epoch = argvs[2]
beam = argvs[3]

if beam == "beam":
    beam = True
else:
    beam = False

EMBED_SIZE = 300
HIDDEN_SIZE = 150
BATCH_SIZE = batch
EPOCH_NUM = epoch
FLAG_GPU = False
tagger = MeCab.Tagger("-Owakati")


def maxlen(minibatch):
    enc_words = [row[0] for row in minibatch]
    enc_max = np.max([len(row) for row in enc_words])

    return enc_max


input_data = pd.read_table("./dataset_small/Utterance_id.txt", encoding='utf-16', header=None)
output_data = pd.read_table("./dataset_small/Response_id.txt", encoding='utf-16', header=None)
input_dataset = []
output_dataset = []
dataset = []

for index, row in input_data.iterrows():
    input_dataset.append(row[0])

for index_out, row in output_data.iterrows():
    output_dataset.append(row[0])

for i in range(len(input_dataset)):
    dataset.append([input_dataset[i].split(","), output_dataset[i].split(",")])

max = maxlen(dataset)
del dataset
del input_data
del input_dataset
del output_data
del output_dataset

f = open("./dataset_small/dictionary.json", 'r')
mydict = json.load(f)
# 語彙数
vocab_size = len(mydict.keys())

model = Seq2Seq(vocab_size=vocab_size,
                embed_size=EMBED_SIZE,
                hidden_size=HIDDEN_SIZE,
                batch_size=1,
                flag_gpu=FLAG_GPU)

if FLAG_GPU:
    ARR = cuda.cupy
    cuda.get_device(0).use()
    model.to_gpu(0)
else:
    ARR = np

PATH = './model/result_EMBED%s_HIDDEN%s_BATCH%s_EPOCH%s.weights' \
       % (EMBED_SIZE, HIDDEN_SIZE, BATCH_SIZE, EPOCH_NUM)
serializers.load_hdf5(PATH, model)

while 1:
    enc_words = []

    user_word = input('単語を入力　>> ')
    # user_word = "みんなおはよう"
    wakati = tagger.parse(user_word)
    user_word = wakati.split(" ")[:-1]
    enc_words.append(user_word)

    for i in range(len(enc_words)):
        for j in range(len(enc_words[i])):
            if enc_words[i][j] in mydict.keys():
                enc_words[i][j] = mydict[enc_words[i][j]]
            else:
                enc_words[i][j] = -1

    enc_words = np.array([[-1] * (max - len(enc_words[0])) + enc_words[0]])
    enc_words = np.array(enc_words).T
    result = forward_test(enc_words=enc_words,
                          model=model,
                          ARR=ARR,
                          beamsearch=beam)

    ans = []
    for i in range(len(result)):
        for k, l in mydict.items():
            if l == result[i]:
                ans.append(k)
                continue
    print(ans)